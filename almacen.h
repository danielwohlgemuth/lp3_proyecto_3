
#include <errno.h>
#include <pthread.h>

#define BUFSIZE 1024
#define MAXNAME 200

typedef struct {
    int archivo_origen;
    int archivo_destino;
    char nombre_archivo[MAXNAME];
} buffer_t;

int getitem(buffer_t *itemp);
int putitem(buffer_t item); 
int getdone(int *flag);
int setdone(void);
