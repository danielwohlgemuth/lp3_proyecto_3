CC = cc
COPS =
LIBTHREAD = -lpthread
POSIXTHREAD = -D_POSIX_PTHREAD_SEMANTICS
RTLIB = -lrt

all:  parallelfilecopy

parallelfilecopy: parallelfilecopy.c almacen.h almacen.c estadistica.h estadistica.c productor.h productor.c consumidor.h consumidor.c globalerror.h globalerror.c restart.h restart.c imprimirmensaje.c
	$(CC) $(COPS) -o parallelfilecopy parallelfilecopy.c almacen.c estadistica.c productor.c consumidor.c globalerror.c restart.c -lm $(RTLIB) $(LIBTHREAD)

clean:
	rm -f *.o parallelfilecopy
