
#include <pthread.h>
#include <limits.h>

int obtenerEstadistica(int *p_cant_archivos,int *p_cant_directorios, 
    int *p_cant_fifos, int *p_suma, long *min, long *max);
int agregarEstadisticaArchivo(int bytes, long tiempo);
int agregarEstadisticaDirectorio(void);
int agregarEstadisticaFIFO(void);
