
#include "estadistica.h"
#include "globalerror.h"

static int cant_archivos = 0;
static int cant_directorios = 0;
static int cant_fifos = 0;
static int suma = 0;
static long tiempo_min = LONG_MAX;
static long tiempo_max = 0;

static pthread_mutex_t archivo_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t directorio_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t fifo_lock = PTHREAD_MUTEX_INITIALIZER;

int obtenerEstadistica(int *p_cant_archivos, 
        int *p_cant_directorios, 
        int *p_cant_fifos, 
        int *p_suma, 
        long *min, 
        long *max) {
    
    int error;
    
    if (pthread_mutex_lock(&archivo_lock) || pthread_mutex_lock(&directorio_lock) || pthread_mutex_lock(&fifo_lock))
        return seterror(error);
    
    *p_cant_archivos = cant_archivos;
    *p_cant_directorios = cant_directorios;
    *p_cant_fifos = cant_fifos;
    *p_suma = suma;
    *min = tiempo_min;
    *max = tiempo_max;
    
    error = pthread_mutex_unlock(&archivo_lock);
    error = pthread_mutex_unlock(&directorio_lock);
    error = pthread_mutex_unlock(&fifo_lock);
    return seterror(error);
}

int agregarEstadisticaArchivo(int bytes, long tiempo) {
    
    int error;
    
    if (error = pthread_mutex_lock(&archivo_lock))
        return seterror(error);
    
    suma += bytes;
    cant_archivos++;
    
    if(tiempo < tiempo_min)
        tiempo_min = tiempo;
    
    if(tiempo > tiempo_max)
        tiempo_max = tiempo;
    
    error = pthread_mutex_unlock(&archivo_lock);
    return seterror(error);
}

int agregarEstadisticaDirectorio(void) {
    
    int error;   

    if (error = pthread_mutex_lock(&directorio_lock))
        return error; 
        
    cant_directorios++;
    
    return pthread_mutex_unlock(&directorio_lock);
}

int agregarEstadisticaFIFO(void) {
    
    int error;   

    if (error = pthread_mutex_lock(&directorio_lock))
        return error; 
        
    cant_fifos++;
    
    return pthread_mutex_unlock(&directorio_lock);
}
