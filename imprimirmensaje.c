#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

int imprimirMensaje(char *mensaje);

int imprimirMensaje(char *mensaje) {
    static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
    int error;   

    if (error = pthread_mutex_lock(&lock))
        return error; 
        
    printf("%s", mensaje);
    
    return pthread_mutex_unlock(&lock);
}

