
#include "productor.h"
#include "globalerror.h"
#include "almacen.h"
#include "estadistica.h"

#include "imprimirmensaje.c"

int imprimirMensaje(char *mensaje);

static void *productor(void *pargs) {
    
    int error;
    char **args;
    struct stat buf;
    
    args = (char **) pargs;
    
    // Crea la carpeta destino base
    lstat(args[0], &buf);
    if(mkdir(args[1], buf.st_mode ) == -1)
        seterror(errno);
    agregarEstadisticaDirectorio();
        
    error = recorrerDirectorio(args[0], args[1]);
    
    // Imprimir si hubo error
    
    error = setdone();
    
    if (error != ECANCELED || error != 0)
        seterror(error);
        
    return NULL;
}

int inicializarProductor(pthread_t *tproductor, char *directorio_origen, char *directorio_destino) {
    
    int error;
    char **args;
    args = calloc(2, sizeof(char *));
    
    args[0] = directorio_origen;
    args[1] = directorio_destino;
    
    error = pthread_create(tproductor, NULL, productor, args);
    
    return seterror(error);
}

int recorrerDirectorio(char *directorio_origen, char *directorio_destino) {

    int error = 0;
    int cantidad;
    struct stat buf;
    
    buffer_t item;
    DIR *directorio_actual;
    struct dirent *archivo_actual;
    
    int archivo_origen;
    int archivo_destino;
    
    char nombre_origen[MAXNAME];
    char nombre_destino[MAXNAME];
    
    char mensaje[MAXNAME];
    
    directorio_actual = opendir(directorio_origen);
    
    while((archivo_actual = readdir(directorio_actual)) != NULL) {
        
        // Agrega al final del nombre del directorio origen y destino
        // el nombre del archivo actual
        snprintf(nombre_origen, MAXNAME, "%s/%s", directorio_origen, archivo_actual->d_name);
        snprintf(nombre_destino, MAXNAME, "%s/%s", directorio_destino, archivo_actual->d_name);
        
        if(isregular(nombre_origen)) {
            
            // Abre el origen en modo lectura
            if ((item.archivo_origen = open(nombre_origen, O_RDONLY)) == -1) {
                // Failed to open source file
                continue; 
            }
            
            // Para obtener los permisos del archivo (mode_t)
            if(lstat(nombre_origen, &buf) == -1)
                seterror(errno);
                
            // Abre el destino en modo escritura (sobreescribe)
            if ((item.archivo_destino = open(nombre_destino, O_WRONLY | O_TRUNC | O_CREAT, buf.st_mode )) == -1) {
                // Failed to open destination file
                continue;
            }
            
            // Guarda el nombre destino en la estructura
            if (snprintf(item.nombre_archivo, MAXNAME, "%s", nombre_destino) == MAXNAME) {
                // Output filename too long
                continue; 
            }
            
            if (error = putitem(item))
                break;
            
        } else if(isdirectory(nombre_origen)) {
            
            // Excluir "." y ".."
            if(strcmp(archivo_actual->d_name, ".") == 0 || strcmp(archivo_actual->d_name, "..") == 0)
                continue;
            
            if(crearDirectorio(nombre_origen, nombre_destino))
                continue;
            
            sprintf(mensaje, "Directorio creado: %s\n", nombre_destino);
            imprimirMensaje(mensaje);
            
            // Llamada recursiva
            recorrerDirectorio(nombre_origen, nombre_destino);
            
        } else if(isfifo(nombre_origen)) {
            
            if(crearFIFO(nombre_origen, nombre_destino))
                continue;
            
            sprintf(mensaje, "FIFO creado: %s\n", nombre_destino);
            imprimirMensaje(mensaje);
        }
    }
    
    closedir(directorio_actual);
    
    return error;
}

int crearDirectorio(char *nombre_origen, char *nombre_destino) {
    
    struct stat buf;
    
    if(lstat(nombre_origen, &buf) == -1)
        return seterror(errno);
        
    if(mkdir(nombre_destino, buf.st_mode ) == -1);
        seterror(errno);
        
    agregarEstadisticaDirectorio();
    
    return 0;
}

int crearFIFO(char *nombre_origen, char *nombre_destino) {
    
    struct stat buf;
    
    if(lstat(nombre_origen, &buf) == -1)
        return seterror(errno);
        
    if(mkfifo(nombre_destino, buf.st_mode ) == -1);
        seterror(errno);
    
    agregarEstadisticaFIFO();
        
    return 0;
}

int isregular(const char *filename) {
    struct stat buf;
    
    if (lstat(filename, &buf) == -1)
        return 0;
    
    return S_ISREG(buf.st_mode);
}

int isfifo(const char *filename) {
    struct stat buf;

    if (lstat(filename, &buf) == -1)
        return 0;
    
    return S_ISFIFO(buf.st_mode);
}

int isdirectory(const char *filename) {
    struct stat buf;
    
    if (lstat(filename, &buf) == -1)
        return 0;
    
    return S_ISDIR(buf.st_mode);
}
