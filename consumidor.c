
#include "consumidor.h"
#include "globalerror.h"
#include "restart.h"
#include "estadistica.h"

int imprimirMensaje(char *mensaje);

int copyfilepass(buffer_t buffer)  {
    
    int archivo_entrada = buffer.archivo_origen;
    int archivo_salida = buffer.archivo_destino;
    char *nombre_archivo = buffer.nombre_archivo;
    
    int bytes = 0;
    
    bytes = copyfile(archivo_entrada, archivo_salida);
    
    r_close(archivo_entrada);
    r_close(archivo_salida);
    
    return bytes; 
}

static void *consumidor(void *arg) {
    
    int error;
    buffer_t nextitem;
    
    int bytes;
    long tiempo_diferencia;
    
    struct timeval tiempo_inicio;
    struct timeval tiempo_fin;
    
    char mensaje[MAXNAME];

    while(1)  {
        if (error = getitem(&nextitem))
            break;
        
        if (error = gettimeofday(&tiempo_inicio, NULL)) {
            // Failed to get start time
            break;
        }
        
        bytes = copyfilepass(nextitem);
        
        if (error = gettimeofday(&tiempo_fin, NULL)) {
            // Failed to get end time
            break;
        }
        
        tiempo_diferencia = MILLION*(tiempo_fin.tv_sec - tiempo_inicio.tv_sec) +
            tiempo_fin.tv_usec - tiempo_inicio.tv_usec;
    
        agregarEstadisticaArchivo(bytes, tiempo_diferencia);
        
        sprintf(mensaje, "Archivo copiado: %s\n", nextitem.nombre_archivo);
        imprimirMensaje(mensaje);
    }
    
    // ECANCELED: No hay mas items
    if (error != ECANCELED)
        seterror(error);
  
    return NULL;
}

int inicializarConsumidor(pthread_t *tconsumidor) {
   int error;

   error = pthread_create(tconsumidor, NULL, consumidor, NULL);
   return (seterror(error));
}
