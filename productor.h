
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <pthread.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void *productor(void *args);

int inicializarProductor(pthread_t *tproductor, char *directorio_origen, char *directorio_destino);
int recorrerDirectorio(char *directorio_origen, char *directorio_destino);
int crearDirectorio(char *nombre_origen, char *nombre_destino);
int crearFIFO(char *nombre_origen, char *nombre_destino);
int isregular(const char *filename);
int isfifo(const char *filename);
int isdirectory(const char *filename);
