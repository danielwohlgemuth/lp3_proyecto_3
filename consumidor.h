
#include <unistd.h>
#include <errno.h>
#include <math.h>
#include <pthread.h>
#include <sys/time.h>
#include <stdio.h>

#include "almacen.h"

#define MILLION 1000000

int copyfilepass(buffer_t buffer);
static void *consumidor(void *arg);
int inicializarConsumidor(pthread_t *tconsumidor);
