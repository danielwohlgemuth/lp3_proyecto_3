
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>

#include "consumidor.h"
#include "productor.h"
//#include "imprimirmensaje.c"
#include "estadistica.h"

int imprimirMensaje(char *mensaje);

static void imprimirEstadistica(int sig_handler);

int configurarSenial(void);

int main(int argc, char *argv[]) {
    
    int num_consumidores;
    char *directorio_origen;
    char *directorio_destino;
    pthread_t *lista_hilos;
    
    long tiempo_diferencia;
    struct timeval tiempo_inicio;
    struct timeval tiempo_fin;
    
    char mensaje[MAXNAME];
    
    int error = 0;
    int i;
    
    if (argc != 4) {  
        fprintf(stderr, "Uso: %s num_consumidores carpeta_origen carpeta_destino\n", argv[0]);
        return 1;
    }

    num_consumidores = atoi(argv[1]) + 1;
    directorio_origen = argv[2];
    directorio_destino = argv[3];
    
    if(configurarSenial() == -1) {
        // Error al configurar la señal - en parallelfilecopy.main()
        return -1;
    }
    
    sprintf(mensaje, "\nMi PID es: %ld\n\n", getpid());
    imprimirMensaje(mensaje);
    sleep(2); // Tiempo para anotar el PID
    
    lista_hilos = calloc(num_consumidores, sizeof(pthread_t));
    
    if (lista_hilos == NULL)
        return -1;
    
    if (error = inicializarProductor(&lista_hilos[0], directorio_origen, directorio_destino)) {
        // Failed to create thread productor // Cambiar a global error
    }
    
    if (error = gettimeofday(&tiempo_inicio, NULL)) {
        // Failed to get start time
    }
    
    for (i = 1; i <= num_consumidores; i++) {
        if (error = inicializarConsumidor(&lista_hilos[i])) {
            // Failed to create thread consumidor
        }
    }
    
    for(i = 1; i <= num_consumidores; i++) {
        
        pthread_join(lista_hilos[i], NULL);
    }
    
    if (error = gettimeofday(&tiempo_fin, NULL)) {
        // Failed to get end time
    }
    
    // Optener datos de estadistica
    raise(SIGUSR1);
    
    tiempo_diferencia = MILLION*(tiempo_fin.tv_sec - tiempo_inicio.tv_sec) +
        tiempo_fin.tv_usec - tiempo_inicio.tv_usec;
        
    sprintf(mensaje, "Tiempo total (segundos): %f\n", (double) tiempo_diferencia/MILLION);
    imprimirMensaje(mensaje);
        
    pthread_join(lista_hilos[0], NULL);
    
    return 0;
}

static void imprimirEstadistica(int sig_handler) {
    
    int cant_archivos;
    int cant_directorios;
    int cant_fifos;
    int suma;
    long min;
    long max;
    
    char mensaje[MAXNAME];
    
    if(obtenerEstadistica(&cant_archivos, &cant_directorios, 
            &cant_fifos, &suma, &min, &max)) {
        return;
    }
    
    sprintf(mensaje, "\n\tDatos copiados\n"
            "Cantidad de archivos: %d\n"
            "Cantidad de directorios: %d\n"
            "Cantidad de FIFOs: %d\n"
            "Total bytes: %d\n"
            "Tiempo minimo (segundos): %f\n"
            "Tiempo maximo (segundos): %f\n\n", 
        cant_archivos, cant_directorios, cant_fifos, suma, (double) min/MILLION, (double) max/MILLION);
    imprimirMensaje(mensaje);
}
    

// Relaciona la funcion que imprime las estadisticas
// con la señal USR1
int configurarSenial(void) {
    
    struct sigaction act;
    act.sa_handler = imprimirEstadistica;
    act.sa_flags = 0;
    
    if(sigemptyset(&act.sa_mask) == -1 ||
            sigaction(SIGUSR1, &act, NULL) == -1) {
        // Error configurando manejo de señal
        return -1;
    }

    return 0;
}
